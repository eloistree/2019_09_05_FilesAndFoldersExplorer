﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class FilesAndFoldersExplorer
{
    [System.Serializable]
    public class GetPathCollectionCallback {
        public string m_currentDirectoryExplored;
        public int m_leftToExplore;
        public PathsCollection m_pathCollection = new PathsCollection();
        public int m_foundFiles=0;
        public int m_foundFolders=0;

        public void AddFolder(string path)
        {
            m_pathCollection.m_directories.Add(path);
            m_foundFolders++;
        }
        
        public void AddFile(string path)
        {
            m_pathCollection.m_files.Add(path);
            m_foundFiles++;
        }
    }

    public static IEnumerator FilterWithCondition_Or(PathsCollection from, PathsCollection to, params FolderCondition [] conditions )
    {
        for (int i = 0; i < from.m_directories.Count; i++)
        {
            
            for (int j = 0; j < conditions.Length; j++)
            {
                if (conditions[j].IsConditionFitting(from.m_directories[i])) {
                    to.AddDirectory(from.m_directories[i]);
                    Debug.Log(">" + from.m_directories[i]);
                    yield return new WaitForEndOfFrame();
                    break;
                }
            }

        }

    }

    public static IEnumerator GetPathCollection(string directoryPath, bool recursive, GetPathCollectionCallback callback, FolderCondition [] toIgnore)
    {

        if (callback == null)
        {
            Debug.LogError("Call back should not be null here !");
            yield break;
        }

        if (!Directory.Exists(directoryPath))
        {
            Debug.LogError("Directory should be check first !");
            yield break;
        }

        callback.m_pathCollection.m_rootPathUsed = directoryPath;

        Queue<string> folderToExplore = new Queue<string>();
        folderToExplore.Enqueue(directoryPath);

        string currentPath ;
        string [] folders;
        do
        {
            currentPath = folderToExplore.Dequeue();
            callback.m_currentDirectoryExplored = currentPath;
            AddFoldersToCollection(ref callback, currentPath);
            folders = GetFoldersFrom(currentPath);
            if (MustWeIgnoreTheExplorationOfThisFolder(toIgnore, currentPath))
            { }
            else
            {

                folders = AddFoldersToCollection(currentPath, ref callback);
                for (int i = 0; i < folders.Length; i++)
                {
                    folderToExplore.Enqueue(folders[i]);
                }
            }


            if (folders.Length == 0)
            {
                yield return new WaitForEndOfFrame();
                callback.m_leftToExplore = folderToExplore.Count;
                //Debug.Log("Count: "+folderToExplore.Count);
            }

        } while (folderToExplore.Count> 0);


           // AddFilesAndfolders(directoryPath, recursive, callback);
        Debug.Log("Hey mon ami ?!");
        yield break;
    }

    private static string[] GetFoldersFrom(string currentPath)
    {
        string[] folders;
        try
        {
            folders = Directory.GetDirectories(currentPath);
        }
        catch (Exception e)
        {
            Debug.LogWarning("Exception to " + currentPath + " : " + e.Message);
            folders = new string[0];
        }

        return folders;
    }

    private static bool MustWeIgnoreTheExplorationOfThisFolder(FolderCondition[] toIgnore, string currentPath)
    {
     
        for (int i = 0; i < toIgnore.Length; i++)
        {
            if (toIgnore[i].IsConditionFitting(currentPath))
            {
                return true;
            }

        }

        return false;
    }

    private static void AddFilesAndfolders(string directoryPath, bool recursive,   GetPathCollectionCallback callback)
    {
        string[] folders;
        AddFilesToCollection(directoryPath,  ref callback);
        folders = AddFoldersToCollection(directoryPath, ref callback);
        if (recursive) {
            for (int i = 0; i < folders.Length; i++)
            {
                AddFilesAndfolders(folders[i], recursive,  callback);
           //     yield return new WaitForEndOfFrame();
            }
        }

    }

    public static void AddFoldersToCollection( ref GetPathCollectionCallback collection, params string [] folderPath)
    {
        for (int i = 0; i < folderPath.Length; i++)
        {
            collection.AddFolder(folderPath[i]);
        }
    }


    public static string [] AddFoldersToCollection(string directoryPath, ref GetPathCollectionCallback collection)
    {
        string[] paths = GetFoldersFrom(directoryPath);
        for (int i = 0; i < paths.Length; i++)
        {
            collection.AddFolder(paths[i]);
        }
        return paths;
    }

    public static string[] AddFilesToCollection(string directoryPath, ref GetPathCollectionCallback collection)
    {
        string [] paths = GetFoldersFrom(directoryPath);
        for (int i = 0; i < paths.Length ; i++)
        {
            collection.AddFile(paths[i]);
        }
        return paths;
    }
}

