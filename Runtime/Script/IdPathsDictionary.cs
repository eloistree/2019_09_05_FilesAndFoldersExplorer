﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[System.Serializable]
public  class IdPathsDictionary
{

    public Dictionary<string, string> m_idToPath = new Dictionary<string, string>();
    public Dictionary<string, string> m_pathToId = new Dictionary<string, string>();

    public List<string> GetAllPaths()
    {
        return new List<string>(m_idToPath.Values);
    }
    public List<string> GetAllIds()
    {
        return new List<string>(m_idToPath.Keys);
    }

    static char[] SEPARATOR = new char[] { '/','\\'};
    public void Add( string id, string path, bool debugAlreadyAdd=true)
    {
        path= path.ToLower().TrimEnd(SEPARATOR);
        if (IsIdAlreadyRegister(id))
        {
            if(debugAlreadyAdd)
            Debug.LogWarning("Id Already added " + id + ": " + path + "\n" +
                id + ": " + GetCurrentPathForId(id));
        }
        else if (IsPathAlreadyRegister(path))
        {
            if (debugAlreadyAdd)
                Debug.LogWarning("Path Already added " + id + ": " + path + "\n" +
                GetCurrentIdForPath(path) + ": " +path );
        }
        else {
            m_idToPath.Add(id, path);
            m_pathToId.Add(path, id);
        }
    }

    public string GetCurrentIdForPath(string path)
    {
        return m_pathToId[path];
    }
    public string GetCurrentPathForId(string id)
    {
        return m_idToPath[id];
    }

    public bool IsPathAlreadyRegister(string path)
    {
        return m_pathToId.ContainsKey(path);
    }
    public bool IsIdAlreadyRegister(string id)
    {
        return m_idToPath.ContainsKey(id);
    }

    internal void Clear()
    {
        m_idToPath.Clear();
        m_pathToId.Clear();

    }

    internal int GetIdCount()
    {
        return m_idToPath.Keys.Count;
    }

    internal int GetPathCount()
    {
        return m_pathToId.Keys.Count;
    }

    public ListOfPathsWithID CopyPathToIdList() {
        ListOfPathsWithID result = new ListOfPathsWithID();
        foreach (string key in m_idToPath.Keys)
        {
            result.AddPath(key, m_idToPath[key]);
        }
        return result;
    }
}

//[System.Serializable]
//public class FilesAndFoldersRegisterIdFromTime : FilesAndFoldersRegister
//{
//    public UnicodeBasedId m_basedUsedToCompress;
//    //"0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ♭♯♮♩♪♫♬☢☠✈✇☎〒✓❄☗☖♚♛♜♝♞♟♔♕♖♗♘♙";
//    [Header("Debug")]
//    public string m_currentTimeId;
//    public string m_previousTimeId;
//    public string m_maxValue;
//    public int m_index;
//    public string m_timeAndIndexAsAlphaNum;
    
//    public override string GetUniqueId()
//    {
//        m_maxValue = ""+long.MaxValue;
//           DateTime timespan = DateTime.Now;
//        m_currentTimeId = timespan.ToString("yyMMddhhmmssmmm");

//        if (m_currentTimeId.CompareTo(m_previousTimeId)!=0) {
//            m_previousTimeId = m_currentTimeId;
//            m_index = 0;
//        }
//        long uniqueId = long.Parse(m_currentTimeId + (m_index++).ToString() );
//        string fullId = m_basedUsedToCompress.Convert(uniqueId);
//        m_timeAndIndexAsAlphaNum = fullId;
//        return fullId;
//    }
  
    
//}