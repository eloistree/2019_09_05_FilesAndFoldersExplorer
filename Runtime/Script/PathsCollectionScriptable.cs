﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PathCollections", menuName = "Scriptable / Paths Collectoin", order = 1)]
public class PathsCollectionScriptable : ScriptableObject
{
    public PathsCollection m_pathCollection;
}

[System.Serializable]
public class PathsCollection {

    public string m_rootPathUsed;
    public List<string> m_directories = new List<string>();
    public List<string> m_files = new List<string>();

    public void AddDirectory(string pathDirectory)
    {
        m_directories.Add(pathDirectory);
    }
    public void AddFile(string pathFile)
    {
        m_files.Add(pathFile);
    }
}
