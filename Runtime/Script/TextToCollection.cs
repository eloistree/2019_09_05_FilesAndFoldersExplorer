﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

public class TextToCollection : MonoBehaviour
{
    public TextAsset m_folders;
    public TextAsset m_files;

    public PathsCollection m_pathCollectionFull = new PathsCollection();


    public PathsCollection m_pathCollectionFiltered = new PathsCollection();

    void Start()
    {
        if(m_folders)
            m_pathCollectionFull.m_directories = m_folders.text.Split('\n').ToList();

        if (m_files)
            m_pathCollectionFull.m_files = m_files.text.Split('\n').ToList();

        StartCoroutine( FilesAndFoldersExplorer.FilterWithCondition_Or(m_pathCollectionFull,  m_pathCollectionFiltered, new UnityProjectStructure()));
    }
    
}
public class UnityProjectStructure : FolderCondition
{
    public override bool IsConditionFitting(string folderPath)
    {
        bool containAsset = Directory.Exists(folderPath+"/Assets") ;
        bool containProject = Directory.Exists(folderPath + "/ProjectSettings");
        return containAsset && containProject;
    }
}