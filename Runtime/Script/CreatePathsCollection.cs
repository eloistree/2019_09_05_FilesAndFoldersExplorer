﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class CreatePathsCollection : MonoBehaviour
{

    public string m_directoryPath;
    public bool m_useRecusrivity;
    public PathsCollectionScriptable m_whereToStore;
    public List<FilesAndFoldersExplorer.GetPathCollectionCallback> m_result = new List<FilesAndFoldersExplorer.GetPathCollectionCallback>();
    public FilesAndFoldersExplorer.GetPathCollectionCallback m_current;
    public int m_filesFound;
    public int m_foldersFound;
    public string m_relativeFileNamesForFolders="AllFolders.txt";
    public List<string> m_allPaths = new List<string>();

    public Text m_debug;
    public InputField m_found;


    private void Start()
    {
       StartCoroutine( Explore());
    }

    public void SetDirectoryPath(string path)
    {
        m_directoryPath = path;

    }
    public void SetDirectoryPathAndStartExploring(string path)
    {
        m_directoryPath = path;
        StartExploring();

    }

    public void Update()
    {
        if (m_debug && m_current != null)
            m_debug.text = string.Format("F{0} D{1} : {2}", m_current.m_foundFiles, m_current.m_foundFolders, m_current.m_currentDirectoryExplored);
    }

    public void StartExploring() {

        StartCoroutine(Explore());
    }

    IEnumerator Explore()
    {
        string filePath = m_directoryPath + "/allfolders.txt";


        if (!File.Exists(filePath))
            File.Create(filePath);
        else File.WriteAllText(filePath, "");

        string[] folders = Directory.GetDirectories(m_directoryPath);
        m_allPaths.Clear();
        m_found.text = "Folder:" + m_allPaths.Count;
        for (int i = 0; i < folders.Length; i++)
        {
            m_current = new FilesAndFoldersExplorer.GetPathCollectionCallback();
            m_result.Add(m_current);
            yield return FilesAndFoldersExplorer.GetPathCollection(folders[i], m_useRecusrivity, m_current, new FolderCondition[] {
                new SimpleFolderCondition(".git"),
                new SimpleFolderCondition("Library"),
                new SimpleFolderCondition("Build"),
                new SimpleFolderCondition(".vs") });

            m_allPaths.AddRange(m_current.m_pathCollection.m_directories);
           
            yield return new WaitForEndOfFrame();

        }
        foreach (string item in m_allPaths)
        {
            m_found.text = m_found.text + "\n" + item;
             File.AppendAllText(filePath, item + "\n");
        }

        //        m_whereToStore.m_pathCollection = m_result.m_pathCollection;
        //#if UNITY_EDITOR
        //        EditorUtility.SetDirty(m_whereToStore);
        //#endif
    }
}


public abstract class FolderCondition {

    public abstract bool IsConditionFitting(string folderPath);
}

public class SimpleFolderCondition : FolderCondition {

    public string m_folderNameToIgnore;

    public SimpleFolderCondition(string folderName)
    {
        this.m_folderNameToIgnore = folderName;
    }

    public override bool IsConditionFitting(string folderPath)
    {
       return Path.GetDirectoryName(folderPath).ToLower().IndexOf(m_folderNameToIgnore.ToLower())>-1;
    }
}

public class GroupOfAnnexedFolder : SimpleFolderCondition
{
    
    public string[] m_foldersPresent;

    public GroupOfAnnexedFolder(string folderName,  params string[] foldersPresent): base(folderName)
    {
        this.m_foldersPresent = foldersPresent;
    }

    public override bool IsConditionFitting(string folderPath)
    {
        bool isOfFolderType = base.IsConditionFitting(folderPath);
        if (isOfFolderType == false)
            return false;

        string[] folderInDirectory = Directory.GetDirectories( folderPath + "/..");

        Debug.Log(folderPath +"\n  "+string.Join(" \n ", folderInDirectory));

        return true;
    }
}

public class MinLevelFolder : FolderCondition
{

    public int inclusiveMinLevel;
    public MinLevelFolder(int minLevel)
    {
        this.inclusiveMinLevel = 0;
    }
   

    public override bool IsConditionFitting(string folderPath)
    {
        int level = folderPath.Split('/', '\\').Length;
        return level >= inclusiveMinLevel ;
    }

}
public class MaxLevelFolder : FolderCondition
{
    
    public int inclusiveMaxLevel;
    public MaxLevelFolder(int maxLevel)
    {
        this.inclusiveMaxLevel = maxLevel;
    }
  

    public override bool IsConditionFitting(string folderPath)
    {
        int level = folderPath.Split('/', '\\').Length;
        return level <= inclusiveMaxLevel;
    }

}