﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TextToCollectionToFolderStructure : MonoBehaviour
{
    public TextAsset m_folders;

    public PathsCollection m_pathCollectionFull = new PathsCollection();


    public PathsCollection m_pathCollectionFiltered = new PathsCollection();

    void Start()
    {
        if (m_folders)
            m_pathCollectionFull.m_directories = m_folders.text.Split('\n').ToList();

    }
}



public abstract class FolderLinks {

    public Dictionary<string, FolderLinks> m_folderPoints = new Dictionary<string, FolderLinks>();

    public abstract FolderLinks GetParentLink();
    public abstract List<FolderLinks> GetChildrens();
    public abstract List<FolderLinks> GetNeighbours();
}