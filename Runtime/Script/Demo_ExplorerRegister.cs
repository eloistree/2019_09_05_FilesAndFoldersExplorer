﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;

public class Demo_ExplorerRegister : MonoBehaviour
{
    
    public RawPaths m_rawPaths;
    public RawPathsLinkedToID m_rawPathsID;
    public RawPathsLinkedToID m_rawPathsFromDictionaryID;
    public NeighborhoodIdRefOfPaths m_neighborhoodReferences;
    public string[] m_ignoreCondition = new string[] { ".git", "Library", "Build", ".vs" };

    //TODO BUG NOT CORRECTED BECAUSE OF UNICODE CHANGE 
    //public UnicodeBasedIdGenerator m_unicodeIdGenerator;


    public IdPathsDictionary m_dictionary = new IdPathsDictionary();
    public NeighborhoodPathDictionary m_neighborhoodDictionary = new NeighborhoodPathDictionary();

    [Header("Option")]
    public bool m_useRecusrvieSearch=true;

    [Header("Debug")]
    public FilesAndFoldersExplorer.GetPathCollectionCallback callback;
    [Header("UI")]
    public InputField m_directoryTargeted;
    public InputField m_rawPathsFilePath;
    public InputField m_rawPathsIDFilePath;
    public InputField m_neighborhoodFilePath;

    [Header("UI Filter")]
    public InputField m_filterByName;
    public InputField m_filterResult;

    [Header("UI Dico")]
    public InputField m_idPathDico;
    public InputField m_idPathDicoResult;


    public GroupOfId m_wordFilterResult;
   

    public void Awake()
    {
        if (string.IsNullOrEmpty(m_directoryTargeted.text))
            m_directoryTargeted.text = Application.dataPath;
        if (string.IsNullOrEmpty(m_rawPathsFilePath.text))
            m_rawPathsFilePath.text = Application.dataPath+"/rawPaths.txt";
        if (string.IsNullOrEmpty(m_rawPathsIDFilePath.text))
            m_rawPathsIDFilePath.text = Application.dataPath + "/rawPathsId.txt";
        if (string.IsNullOrEmpty(m_neighborhoodFilePath.text))
            m_neighborhoodFilePath.text = Application.dataPath + "/rawPathsNeighborhood.txt";
    }

    public void LoadDirectoryToPaths()
    {

        StartCoroutine( StartExploringDirecotry());

    }

    private IEnumerator StartExploringDirecotry()
    {
        m_rawPaths.m_data.Clear();
        yield return FilesAndFoldersExplorer.GetPathCollection(m_directoryTargeted.text, m_useRecusrvieSearch, callback, GetFolderToIgnore());
        m_rawPaths.m_data.AddPaths(callback.m_pathCollection.m_directories);
        m_rawPaths.m_data.SaveInFile(m_rawPathsFilePath.text);

    }

   

    public void GeneratePathsId() {

        m_rawPathsID.m_data.Clear();
        // m_unicodeIdGenerator
        List<string> paths = m_rawPaths.m_data.m_paths;
        for (int i = 0; i < paths.Count; i++)
        {

            throw new System.NotImplementedException("Bug because of rush");
            //m_rawPathsID.m_data.AddPath(m_unicodeIdGenerator.GetUniqueId(), paths[i]);
        }
        m_rawPathsID.m_data.SaveInFile(m_rawPathsIDFilePath.text);

    }

    public void LoadPathsIdToDictionary() {

        m_dictionary.Clear();

        for (int i = 0; i < m_rawPathsID.m_data.m_paths.Count; i++)
        {
            m_dictionary.Add(m_rawPathsID.m_data.m_paths[i].m_id, m_rawPathsID.m_data.m_paths[i].m_paths, false);
        }
        Debug.Log("Hey hey :)  Origne: "+ m_rawPathsID.m_data.m_paths.Count+ " - "+ + m_dictionary .GetIdCount()+ "vs"+ m_dictionary.GetPathCount());
       // m_rawPathsFromDictionaryID.m_data = m_dictionary.CopyPathToIdList();
      
    }
    public void SaveDictionaryToRawPath() {
        m_rawPathsFromDictionaryID.m_data = m_dictionary.CopyPathToIdList();
    }


    public void GenerateWithoutExplorationNeighborhood() {

        m_neighborhoodReferences.m_data.Clear();
        List<PathWithId> paths = m_rawPathsFromDictionaryID.m_data.m_paths;
        for (int i = 0; i < paths.Count; i++)
        {
            m_neighborhoodReferences.m_data.AddWithoutInfo(paths[i].m_id);
        }
        m_neighborhoodDictionary = m_neighborhoodReferences.m_data.CopyAsDictionary();
        m_neighborhoodReferences.m_data.SaveInFile(m_neighborhoodFilePath.text);
    }



    private FolderCondition[] GetFolderToIgnore()
    {
        FolderCondition[] conditions = new FolderCondition[m_ignoreCondition.Length];
        for (int i = 0; i < m_ignoreCondition.Length; i++)
        {
            conditions[i] = new SimpleFolderCondition(m_ignoreCondition[i]);

        }
        return conditions;
    }

    public void CheckIdOnDictionary() {
        if (m_dictionary.IsIdAlreadyRegister(m_idPathDico.text))
            m_idPathDicoResult.text = m_dictionary.GetCurrentPathForId(m_idPathDico.text);
        else m_idPathDicoResult.text = "None";



    }

    public void CreateGroupFromWords() {

        string lookFor = m_filterByName.text.ToLower();
        List<PathWithId> paths = m_rawPathsFromDictionaryID.m_data.m_paths;
        string folderName = "";
        string test="";
        for (int i = 0; i < paths.Count; i++)
        {
            string[] tokens = paths[i].m_paths.Split(new char[] { '/', '\\' });
            if (tokens.Length == 0)
                folderName = "";
            else
                folderName = tokens[tokens.Length-1];
            if (i < 10)
                test += "  " + folderName;
            if (!string.IsNullOrEmpty(folderName) && folderName== lookFor) {
                m_wordFilterResult.Clean();
                m_wordFilterResult.Add(paths[i].m_id);
            }
        }
        Debug.Log("t: " + test);
        m_filterResult.text = string.Join(" ", m_wordFilterResult.GetGroup());

    }


}
