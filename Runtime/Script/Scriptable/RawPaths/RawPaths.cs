﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;


[CreateAssetMenu(fileName = "RawPath", menuName = "Scriptable / RawPaths", order = 1)]
public class RawPaths : ScriptableObject
{
    public ListOfPaths m_data;
//    public void SetDirty() {
//#if UNITY_EDITOR
//        UnityEditor.EditorUtility.SetDirty(this);
//#endif
//        Debug.Log("I am dirty !", this);
//    }
}
[System.Serializable]
public class ListOfPaths {
    public List<string> m_paths = new List<string>();
    public void AddPath(string path)
    {
        this.m_paths.Add(path);
    }

    public void Clear()
    {
        m_paths.Clear();
    }

    public void AddPaths(IEnumerable<string> paths) {
        this.m_paths.AddRange(paths.Where(k => !string.IsNullOrEmpty(k)));
    }


    public void SaveInFile(string filePath)
    {
        File.WriteAllText(filePath, SaveAsText());

    }
    public void LoadFromFile(string filePath)
    {
        ImportFromText(File.ReadAllText(filePath));

    }


    public string SaveAsText()
    {
        string txt = "";
        for (int i = 0; i < m_paths.Count; i++)
        {
            txt += string.Format("{0}\n",m_paths[i]);
        }
        return txt;
    }
    public void ImportFromText(string text)
    {
        string[] tokens = text.Split('\n');
        AddPaths(tokens);
    }
}
