﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;


[CreateAssetMenu(fileName = "RawPathWithID", menuName = "Scriptable / RawPaths linked to ID", order = 1)]
public class RawPathsLinkedToID : ScriptableObject
{
    public ListOfPathsWithID m_data;
}
[System.Serializable]
public class ListOfPathsWithID
{
    public List<PathWithId> m_paths = new List<PathWithId>();

    public void AddPath(string id, string path)
    {
        this.m_paths.Add(new PathWithId(id, path));
    }


    public void SaveInFile(string filePath)
    {
        File.WriteAllText(filePath, SaveAsText());

    }
    public void LoadFromFile(string filePath)
    {
        ImportFromText(File.ReadAllText(filePath));

    }


    public string SaveAsText() {
        string txt = "";
        for (int i = 0; i < m_paths.Count; i++)
        {
            txt += string.Format("{0} | {1}\n", m_paths[i].m_id, m_paths[i].m_paths);

        }
        return txt;
    }
    public void ImportFromText(string text) {
        string[] tokens = text.Split('\n');
        string [] linetokens;
        for (int i = 0; i < tokens.Length; i++)
        {
            linetokens = Regex.Split(tokens[i], " | ");
            if(linetokens.Length==2)
            AddPath(linetokens[0], linetokens[1]);
        }


    }

    public void Clear()
    {
        m_paths.Clear();
    }
}


[System.Serializable]
public class PathWithId
{
    public string m_id;
    public string m_paths;

    public PathWithId(string id, string path)
    {
        this.m_id = id;
        this.m_paths = path;
    }
}