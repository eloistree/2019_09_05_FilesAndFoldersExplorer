﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

[CreateAssetMenu(fileName = "NeighborhoodRefs", menuName = "Scriptable / Neighborhood References of Paths", order = 1)]
public class NeighborhoodIdRefOfPaths : ScriptableObject
{
    // ID || PARENTS || TOP | Near | CHILD || FILES
    // 互亓 || 仓仔<仳仴<伣伤伥 || 亱亲亳  亴亵  亶 |  乢  乣乤  乥 | 亓  五  井  亖  亗 || 令以 仦仧
    // Start is called before the first frame update
    public NeighborhoodList m_data;

}

public class NeighborhoodPathDictionary
{
    public Dictionary<string, NeighborhoodsOfPath> m_dictionary = new Dictionary<string, NeighborhoodsOfPath>();

    public void Add(NeighborhoodsOfPath pathInfo, bool debugWarning=true)
    {
        string id= pathInfo.m_id;
        if (m_dictionary.ContainsKey(id))
        {
            if(debugWarning)
              Debug.LogWarning("The current id ("+id+") is already present");
        }
        else {
            m_dictionary.Add(id, pathInfo);
        }
    }
}
[System.Serializable]
public class NeighborhoodList
{
    public List<NeighborhoodsOfPath> m_neighborhoods = new List<NeighborhoodsOfPath>();

    public void AddWithoutInfo(string id)
    {
        m_neighborhoods.Add(new NeighborhoodsOfPath() { m_id = id});
    }
    public void Add(NeighborhoodsOfPath info) {
        m_neighborhoods.Add(info);
    }
    public NeighborhoodPathDictionary CopyAsDictionary() {
        NeighborhoodPathDictionary dicoResult = new NeighborhoodPathDictionary();
        for (int i = 0; i < m_neighborhoods.Count; i++)
        {
            dicoResult.Add(m_neighborhoods[i]);

        }
        return dicoResult;
    }

    public  void Clear()
    {
        m_neighborhoods.Clear();
    }

  
    public void SaveInFile(string filePath)
    {
        File.WriteAllText(filePath, SaveAsText());

    }
    public void LoadFromFile(string filePath)
    {
        ImportFromText(File.ReadAllText(filePath));

    }


    public string SaveAsText()
    {
        string result = "";
        for (int i = 0; i < m_neighborhoods.Count; i++)
        {
            result += m_neighborhoods[i].GetOneLinerString() + "\n";
        }
        return result;
    }
    public void ImportFromText(string text)
    {
        throw new System.NotImplementedException();
        //string[] tokens = text.Split('\n');
        //string[] linetokens;
        //for (int i = 0; i < tokens.Length; i++)
        //{
        //    linetokens = Regex.Split(tokens[i], " | ");
        //    if (linetokens.Length == 2)
        //        AddPath(linetokens[0], linetokens[1]);
        //}


    }

}
[System.Serializable]
public class NeighborhoodsOfPath {

    public string m_id="";
    public string[] m_parentsRootToLeaf=new string[0];
    public string[] m_directParentNeighborhood = new string[0];
    public string[] m_sideNeighborhood = new string[0];
    public string[] m_childNeighborhood = new string[0];
    public string[] m_files = new string[0];

    public void SetId (string id){
        m_id = id;
    }
    public string GetOneLinerString() { return string.Format("{0}||{1}||{2}|{3}|{4}||{5}", m_id,
        string.Join(" ", m_parentsRootToLeaf),
        string.Join(" ", m_directParentNeighborhood),
        string.Join(" ", m_sideNeighborhood),
        string.Join(" ", m_childNeighborhood),
        string.Join(" ", m_files)
        ); }
    public void SetFromOneLiner(string line) {
        
    }
}

public class NeighborhoodsOfPathUtility {



}