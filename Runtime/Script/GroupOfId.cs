﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GroupOfId
{
    [SerializeField] List<string> m_ids = new List<string>();
    public void Clean() { }

    public void Add(string id) { m_ids.Add(id); }
    public string[] GetGroup() { return m_ids.ToArray(); }
}
