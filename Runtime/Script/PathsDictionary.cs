﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathsDictionary 
{

    public string SaveAsText() {
        string result="";
        return result;
    }
    public void LoadSavedText(string text) {

    }

    public void SaveAsFile(string filePath)
    {

    }
    public void LoadFromFile(string filePath)
    {

    }
}

public class StringPath {
    public static char[] SEPARATOR = new char [] { '/','\\'};
     string m_path;

    public StringPath(string path)
    {
        this.m_path = path;
    }
    public string GetPath() { return m_path; }
    public string [] GetTokens() { return m_path.Split(SEPARATOR); }
}

public class StringPathWarpper : StringPathCommonRequest{
    public StringPath m_path;

    public StringPathWarpper(string path)
    {
        this.m_path = new StringPath( path);
    }

    public bool GetWindowDisk(out char diskId)
    {
        throw new System.NotImplementedException();
    }

    public bool IsAbsolutePath()
    {
        throw new System.NotImplementedException();
    }

    public bool IsAndroidPath()
    {
        throw new System.NotImplementedException();
    }

    public string IsDirectory()
    {
        throw new System.NotImplementedException();
    }

    public string IsFile()
    {
        throw new System.NotImplementedException();
    }

    public bool IsRelativePath()
    {
        throw new System.NotImplementedException();
    }

    public bool IsWebPath()
    {
        throw new System.NotImplementedException();
    }

    public bool IsWindowPath()
    {
        throw new System.NotImplementedException();
    }
}

public interface StringPathCommonRequest {

    bool IsWindowPath();
    bool IsAndroidPath( );
    bool IsWebPath(     );
    bool IsRelativePath();
    bool IsAbsolutePath();
    string IsDirectory( );
    string IsFile(      );
    bool GetWindowDisk(out char diskId);
}

public class PathKeyValue {

    public string m_id;
    public StringPath m_path;
    public string GetId() { return m_id; }
    public string GetPath() { return m_path.GetPath(); }
}
public class PathKeyOnly {
    public string m_id;
    public string GetId() { return m_id; }
}

public abstract class PathIdLink {
    PathKeyValue m_currentPath;
    protected PathIdLink(PathKeyValue currentPath)
    {
        this.m_currentPath = currentPath;
    }
    public PathKeyValue GetCurrentPath() { return m_currentPath; }
    public string GetCurrentPathID() { return m_currentPath.GetId(); }
}

//public class PathRecusriveUpDownLink : PathIdLink
//{
//    public PathRecusriveUpDownLink m_up;
//    public PathRecusriveUpDownLink m_down;

//}
//public class PathStaticUpDownLink : PathIdLink
//{
//    public PathKeyOnly[] m_rootDirection;
//    public PathKeyOnly[] m_leafDirection;

//}
//public class PathsChildensLink
//{
//    public string ;

//}
//public class PathsneighborhoodLink
//{
//    public string ;

//}